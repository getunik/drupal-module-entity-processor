# Overview
It is essentially just a centralized implementation of various entity processing hooks that allows other modules to provide specific processing code targeted at the entity type or bundle level. The same bundle can easily be targeted for processing by multiple modules or multiple different processors of the same module, allowing a more modularized approach to entity processing.

The primary purposes of the entity processor abstraction are:
* reducing the Drupal boilerplate code usually required for process, preprocess, etc. functions
* allow easy structuring of processing code (instead of doing everything in one big function)
  * allow processing functions defined in classes
  * separate processors per entity type AND bundle

Custom processors can be registered with the HOOK_entity_processor_bundle_settings:

```php
function HOOK_entity_processor_bundle_settings()
{
	return array(
		'entity-type-key' => array(
			'bundle-key' => array(
				'pipeline-name' => array(
					... processor definitions ...
				),
				...
			),
			...
		),
		...
	);
}
```

# Capabilities

The following hooks are currently supported:
* hook_entity_load (as pipeline **load**) with one call per entity being loaded instead of an entities array
* hook_preprocess (as pipeline **preprocessor**)
* hook_process (as pipeline **processor**)
* hook_entity_view (as pipeline **view**)

The arguments of the processor functions are exactly the same as the hook implementations, except that additional arguments may be defined when registering a processor.

## Defining a processor

### hook / function
```php
use Drupal\entity_processor\ProcessorFunction;

new ProcessorFunction('your_function_name')
	->addArg('additional arguments')
	->addArg('for the processor function')
```

### class method
```php
use Drupal\entity_processor\ProcessorClass;

(new ProcessorClass('YourClassName', 'yourMethodName'))
	->addArg('additional arguments')
	->addArg('for the processor method')
```

## The '#all' entity type and bundle
If a specific processor (or set of processors) should be applied to all entity types or all bundles of an entity type, then the special string `#all` can be used as the entity type or bundle key.

## The special theme_hook_suggestions pipeline
TODO

# Example
```php
use Drupal\entity_processor\ProcessorClass;
use Drupal\entity_processor\ProcessorFunction;
use Drupal\entity_processor\SuggestionChain;
use Drupal\entity_processor\LinkFixed;
use Drupal\entity_processor\LinkField;
use Drupal\entity_processor\LinkSpecial;


function HOOK_entity_processor_bundle_settings()
{
	return array(
		'#all' => array(
			'preprocessor' => array(
				(new ProcessorFunction('MYMODULE_arbitrary_function_name'))
					->addArg(42),
			),
		),
		'content_fragment' => array(
			'#all' => array(
				'preprocessor' => array(
					(new ProcessorClass('MyCustomProcessor', 'preprocess'))
						->addArg('test'),
				),
				'view' => array(
					new ProcessorClass('MyCustomProcessor', 'view'),
				),
			),
			'block' => array(
				'theme_hook_suggestions' => array(
					(new SuggestionChain())
						->add(new LinkFixed('prefix'))
						->add(new LinkSpecial('entity_type'))
						->add(new LinkSpecial('bundle'))
						->add(new LinkField('field_some_field_name')),
				),
			),
		)
	);
}
```
