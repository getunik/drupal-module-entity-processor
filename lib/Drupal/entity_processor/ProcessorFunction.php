<?php

namespace Drupal\entity_processor;

/**
 * Represents a simple global processor $function (function name)
 */
class ProcessorFunction extends ProcessorDefinition
{
	protected $function;

	public function __construct($function)
	{
		parent::__construct();
		$this->function = $function;
	}

	protected function executeInternal($args)
	{
		call_user_func_array($this->function, $args);
	}
}
