<?php

namespace Drupal\entity_processor;

/**
 * Represents a class method processor defined by $name (fully qualified class name) and $method
 */
class ProcessorClass extends ProcessorDefinition
{
	protected $class;
	protected $method;

	public function __construct($class, $method)
	{
		parent::__construct();
		$this->class = $class;
		$this->method = $method;
	}

	protected function executeInternal($args)
	{
		$instance = new $this->class();
		call_user_func_array(array($instance, $this->method), $args);
	}
}
