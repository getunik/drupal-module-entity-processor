<?php

namespace Drupal\entity_processor;

/**
 * Fixed string suggestion link
 */
class LinkFixed extends ChainLink
{
	protected $name;

	public function __construct($name)
	{
		$this->name = $name;
	}

	public function getValue($wrapper, &$vars)
	{
		return $this->name;
	}
}
