<?php

namespace Drupal\entity_processor;

/**
 * Special suggestion link whose value is taken from the preprocessor pipeline variables.
 * The $name for the constructor is the array key of the variables array .
 */
class LinkSpecial extends ChainLink
{
	protected $name;

	public function __construct($name)
	{
		$this->name = $name;
	}

	public function getValue($wrapper, &$vars)
	{
		if (isset($vars[$this->name]) && !empty($vars[$this->name]))
		{
			return $vars[$this->name];
		}

		return NULL;
	}
}
