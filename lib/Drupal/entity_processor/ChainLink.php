<?php

namespace Drupal\entity_processor;

/**
 * Base class for @see SuggestionChain links
 */
abstract class ChainLink
{
	abstract public function getValue($wrapper, &$vars);
}
