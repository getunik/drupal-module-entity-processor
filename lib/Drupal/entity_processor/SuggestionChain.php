<?php

namespace Drupal\entity_processor;

/**
 * This configuration object represents a chain of theme hook suggestions in the form
 * of "links" (implementations of the @see ChainLink class).
 */
class SuggestionChain
{
	protected $updateExisting;
	protected $links;

	public function __construct($updateExisting = TRUE)
	{
		$this->updateExisting = $updateExisting;
		$this->links = array();
	}

	public function add(ChainLink $link)
	{
		$this->links[] = $link;
		return $this;
	}

	public function updateSuggestions(&$suggestions, $wrapper, $variables)
	{
		$segments = array();
		foreach ($this->links as $link)
		{
			$value = $link->getValue($wrapper, $variables);
			if ($value !== NULL)
			{
				$segments[] = strtolower(preg_replace('/[^a-zA-Z0-9_]+/', '_', $value));

				$suggestion = implode('__', $segments);
				$index = array_search($suggestion, $suggestions);

				if ($index !== FALSE)
				{
					// the suggestion already exists in the $suggestions array. what we do now depends
					// on the setting for $updateExisting
					if ($this->updateExisting)
					{
						// make sure that pre-existing suggestions that are the same as new ones get "updated"
						array_splice($suggestions, $index, 1);
					}
					else
					{
						// skip this suggestion without updating it
						continue;
					}
				}

				$suggestions[] = $suggestion;
			}
		}
	}
}
