<?php

namespace Drupal\entity_processor;

/**
 * Field value suggestion link. Extracts the value of the field given by $name and uses
 * that value as the suggestion link.
 */
class LinkField extends ChainLink
{
	protected $name;

	public function __construct($name)
	{
		$this->name = $name;
	}

	public function getValue($wrapper, &$vars)
	{
		if (isset($wrapper->{$this->name}) && !empty($wrapper->{$this->name}->value()))
		{
			return $wrapper->{$this->name}->value();
		}

		return NULL;
	}
}
