<?php

namespace Drupal\entity_processor;

/**
 * Base class for processor definitions. It prepares all arguments so that the
 * actual implementation only has to execute the underlying processor code.
 */
abstract class ProcessorDefinition
{
	protected $customArgs;

	public function __construct()
	{
		$this->customArgs = array();
	}

	public function addArg($value)
	{
		$this->customArgs[] = $value;
		return $this;
	}

	public function execute($baseArgs)
	{
		$args = array_merge($baseArgs, $this->customArgs);
		$this->executeInternal($args);
	}

	abstract protected function executeInternal($args);
}
